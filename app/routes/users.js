const express = require('express');
const { createUser, getUsers, getUser, deleteUser, updateUser } = require('../controllers/users');
const { validateCreateUser } = require('../validators/users');
const router = express.Router();

// Crear usuario: POST /users -> en el body el usuario json
router.post('/users', validateCreateUser, createUser);

// Recuperar todos: GET /users
router.get('/users', getUsers);

// Recuperar por id: GET /users/:id
router.get('/users/:id', getUser);

// Borrado de usuario por id: DELETE /users/:id
router.delete('/users/:id', deleteUser);

// Actualizar usuario: PUT /users/:id -> en el body el nuevo usuario
router.put('/users/:id', updateUser);

module.exports = router;