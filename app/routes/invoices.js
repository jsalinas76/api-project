const express = require('express');
const { createInvoice, getInvoices, getInvoice, deleteInvoice, updateInvoice } = require('../controllers/invoices');
const { validateCreateInvoice } = require('../validators/invoices');
const router = express.Router();

// Crear factura: POST /invoices -> en el body la factura json
router.post('/invoices', validateCreateInvoice, createInvoice);

// Recuperar todas: GET /invoices
router.get('/invoices', getInvoices);

// Recuperar por id: GET /invoices/:id
router.get('/invoices/:id', getInvoice);

// Borrado de factura por id: DELETE /invoices/:id
router.delete('/invoices/:id', deleteInvoice);

// Actualizar factura: PUT /invoices/:id -> en el body la nueva factura
router.put('/invoices/:id', updateInvoice);

module.exports = router;