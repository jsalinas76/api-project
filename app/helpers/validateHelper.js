const { validationResult } = require("express-validator");

const validateResult = (request, response, next) => {
    const errors = validationResult(request).errors;
    if(errors.length){
        return response.status(400).json({errors: errors});
    }
    return next();
}

module.exports = { validateResult };