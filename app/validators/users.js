const { check } = require("express-validator");
const { validateResult } = require("../helpers/validateHelper");


const validateCreateUser = [
    check('name').exists().isLength({ min: 5}),
    check('age').exists().isNumeric(),
    check('email').exists().isEmail(),
    (request, response, next) => {
        validateResult(request, response, next);
    }
];

module.exports = { validateCreateUser };