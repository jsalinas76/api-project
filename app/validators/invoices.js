const { check, validationResult } = require("express-validator");
const { validateResult } = require("../helpers/validateHelper");

const validateCreateInvoice = [
    check('subject').exists().isLength({ min: 5}),
    check('amount').exists().isNumeric(),
    check('client').exists(),
    (request, response, next) => {
        validateResult(request, response, next);
    }
];

module.exports = { validateCreateInvoice };