const express = require('express');
const compression = require('compression');
const app = express();

app.use(express.json());
app.use(compression());

const port = '3000';

app.use('/api/1.0', require('./app/routes/users'));
app.use('/api/1.0', require('./app/routes/invoices'));

app.listen(port, () => {
    console.log('Servidor ejecutándose en el puerto: ' + port);
});